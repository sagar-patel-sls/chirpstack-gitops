# DavisVantagePro2

## Example of decoded object

### [WeatherObserved](https://fiware-datamodels.readthedocs.io/en/latest/Weather/WeatherObserved/doc/spec/index.html)

        {
                "type": "WeatherObserved",
                "atmosphericPressure": 1001.5583120533432,
                "dayET": 2.5146, // added
                "dayRain": 0.254, // added
                "relativeHumidity": 0.53,
                "temperature": 23.94444444444444,
                "rainRate": 0, // added
                "solarRadiation": 220,
                "windSpeedAvg": 5.36448, // added
                "windDirection": 73,
                "windSpeed": 4.91744
        }

### [AirQualityObserved](https://fiware-datamodels.readthedocs.io/en/latest/Environment/AirQualityObserved/doc/spec/index.html)

        {
                "type": "AirQualityObserved",
                "PM10": 13, // added
                "PM1.0": 13, // added
                "PM2.5": 13 // added
        }
