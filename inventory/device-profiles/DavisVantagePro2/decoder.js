// function reverse(str) {
//   return str
//     .match(/[a-fA-F0-9]{2}/g)
//     .reverse()
//     .join("");
// }

function fahrenheitToCelsius(value) {
  return (value - 32) / 1.8;
}

function getAtmosphericPressure(value) {
  return value * 33.863886666667;
}

function getET(value) {
  return (value / 1000) * 25.4;
}

function getRainRate(value) {
  return value * 0.254;
}

function getWindSpeed(value) {
  return value * 0.44704;
}

// function getWindSpeedAvg(value) {
//   return value * 0.44704;
// }

function getDayRain(value) {
  return value * 0.254;
}

function parseDevisData(payload) {
  var result = {};

  if (payload.length <= 8) {
    throw new Error("Device sent Error");
  }
  if (payload[0] == 0x0b) {
    switch (payload[2]) {
      case 0x00:
        result.type = "WeatherObserved";
        result.atmosphericPressure = getAtmosphericPressure(
          (((payload[6] << 8) & 0x0000ff00) | (payload[5] & 0x000000ff)) /
            1000.0
        );

        result.temperature = fahrenheitToCelsius(
          (((payload[8] << 8) & 0x0000ff00) | (payload[7] & 0x000000ff)) / 10.0
        );

        result.windSpeed = getWindSpeed(payload[9] & 0x00ff);

        result.windSpeedAvg = getWindSpeed(payload[10] & 0x00ff);

        result.windDirection =
          ((payload[12] << 8) & 0x0000ff00) | (payload[11] & 0x000000ff);

        result.relativeHumidity = (payload[13] & 0x00ff) / 100;

        result.rainRate = getRainRate(
          ((payload[15] << 8) & 0x0000ff00) | (payload[14] & 0x000000ff)
        );

        // no sensor installed!
        // result.uv = payload[16] & 0x00ff;

        result.solarRadiation =
          ((payload[18] << 8) & 0x0000ff00) | (payload[17] & 0x000000ff);

        result.dayRain = getDayRain(
          ((payload[20] << 8) & 0x0000ff00) | (payload[19] & 0x000000ff)
        );

        result.dayET = getET(
          ((payload[22] << 8) & 0x0000ff00) | (payload[21] & 0x000000ff)
        );

        break;

      case 0x01:
        result.type = "AirQualityObserved";

        result["PM1.0"] = (payload[7] & 0xff) + ((payload[8] << 7) & 0xff00);

        result["PM2.5"] = (payload[9] & 0xff) + ((payload[10] << 8) & 0xff00);

        result.PM10 = (payload[11] & 0xff) + ((payload[12] << 8) & 0xff00);
        break;
    }
  }
  return result;
}

function Decode(fPort, bytes) { // eslint-disable-line
  return parseDevisData(bytes);
}
