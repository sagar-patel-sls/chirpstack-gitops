package request

import "context"

type APICredentials struct {
	ChirpStackURL   string
	ChirpStackToken string
}

type JwtCredentials struct {
	Token string
}

func (j *JwtCredentials) GetRequestMetadata(ctx context.Context, url ...string) (map[string]string, error) {
	return map[string]string{
		"authorization": j.Token,
	}, nil
}

func (j *JwtCredentials) RequireTransportSecurity() bool {
	return false
}
