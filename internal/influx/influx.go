package influx

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/wobcom/iot/chirpstack-gitops/internal/request"
)

type Response struct {
	Results []struct {
		StatementID int `json:"statement_id"`
		Series      []struct {
			Name    string     `json:"name"`
			Columns []string   `json:"columns"`
			Values  [][]string `json:"values"`
		} `json:"series"`
	} `json:"results"`
}

func DBExists(ac request.Requester, name string) (bool, error) {
	r := Response{}
	params := url.Values{}
	params.Add("q", "SHOW DATABASES")

	status, data, err := ac.Request("GET", "query?"+params.Encode(), nil)
	if err != nil {
		return false, err
	}

	if status != http.StatusOK {
		// nolint: goerr113
		return false, fmt.Errorf("something went wrong %d: %s", status, string(data))
	}

	if err := json.Unmarshal(data, &r); err != nil {
		return false, err
	}

	if len(r.Results) < 1 || len(r.Results[0].Series) < 1 {
		// nolint: goerr113
		return false, fmt.Errorf("got strange results")
	}

	for _, i := range r.Results[0].Series[0].Values {
		if i[0] == name {
			return true, nil
		}
	}

	return false, nil
}

func CreateDB(ac request.Requester, name string) error {
	params := url.Values{}
	params.Add("q", fmt.Sprintf("CREATE DATABASE %s", name))

	status, data, err := ac.Request("POST", "query?"+params.Encode(), nil)
	if err != nil {
		return err
	}

	if status != http.StatusOK {
		// nolint: goerr113
		return fmt.Errorf("something went wrong %d: %s", status, string(data))
	}

	return nil
}
