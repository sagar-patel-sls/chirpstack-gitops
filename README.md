# Chirpstack GitOps Tool

[![wobcom](./wobcom-badge.png)](https://gitlab.com/wobcom)
[![pipeline status](https://gitlab.com/wobcom/iot/chirpstack-gitops/badges/master/pipeline.svg)](https://gitlab.com/wobcom/iot/chirpstack-gitops/-/commits/master)


Acknowledgement
---------------

This is the 2nd version of an internal tool created at Wobcom by @marvin.preuss which shares a lot of the code and ideas.   
Due to clean-up and open sourcing the repository history does not reflect this contribution.  

Decoder development
-------------------

The decoder was developed/tested with https://gitlab.com/wobcom/salamander